<?php

$sitemapFields = [
    'url_path' => [
        'label' => 'URL Path',
        'type'  => 'text',
    ],
];

foreach(config('sitemap.generators') as $identifier => $className) {
    $generator = new $className();
    $sitemapFields['exclude-' . $identifier] = [
        'label'   => 'Exclude ' . $generator->getTitle(),
        'type'    => 'select',
        'source' => \WFN\Sitemap\Model\Source\Yesno::getInstance(),
    ];
}

return [
    'sitemap' => [
        'label'  => 'XML Sitemap',
        'fields' => $sitemapFields,
    ],
];