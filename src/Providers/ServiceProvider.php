<?php
namespace WFN\Sitemap\Providers;

use WFN\Admin\Providers\ServiceProvider as WFNServiceProvider;
use Illuminate\Support\Facades\Route;

class ServiceProvider extends WFNServiceProvider
{

    public function boot(\Illuminate\Routing\Router $router)
    {
        $basePath = realpath(__DIR__ . '/..');
        
        $this->publishes([
            $basePath . '/assets' => public_path('sitemap'),
        ], 'public');

        $this->mergeConfigFrom($basePath . '/Config/sitemap.php', 'sitemap');
        $this->mergeConfigFrom($basePath . '/Config/settings.php', 'settings');

        Route::middleware('web')->group($basePath . '/routes/web.php');
    }

}
