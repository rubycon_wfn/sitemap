<?php

namespace WFN\Sitemap\Model;

abstract class Generator
{

    const MAIN_TAG = 'url';

    const TITLE = '';
    
    public abstract function getLinks();

    public abstract function getLastMod();

    public function getTitle()
    {
        return static::TITLE;
    }

    public function getXml()
    {
        $stylesUrl = url(asset('sitemap/css/sitemap.xsl'));
        $sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<?xml-stylesheet type=\"text/xsl\" href=\"{$stylesUrl}\"?>
        ";
        $sitemap .= $this->_getSitemapStartTag();

        $mainTag = static::MAIN_TAG;
        foreach($this->getLinks() as $link) {
            $sitemap .= "
    <{$mainTag}>
        <loc>{$link->loc}</loc>
        <lastmod>{$link->lastmod->toIso8601String()}</lastmod>
    </{$mainTag}>
            ";
        }

        $sitemap .= $this->_getSitemapEndTag();
        return $sitemap;
    }

    protected function _getSitemapStartTag()
    {
        return "
<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
    xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"
    xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.google.com/schemas/sitemap-image/1.1 http://www.google.com/schemas/sitemap-image/1.1/sitemap-image.xsd\"
    xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
>
        ";
    }

    protected function _getSitemapEndTag()
    {
        return "
</urlset>
        ";
    }

}