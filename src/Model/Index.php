<?php

namespace WFN\Sitemap\Model;

class Index extends Generator
{

    const MAIN_TAG = 'sitemap';
    
    public function getLinks()
    {
        $links = [];
        foreach(config('sitemap.generators') as $generatorName => $generatorClass) {
            $generator = new $generatorClass();
            if(\Settings::getConfigValue('sitemap/exclude-' . $generatorName)) {
                continue;
            }
            $links[] = (object)[
                'loc'     => url(route('sitemap.view', ['generator' => $generatorName])),
                'lastmod' => $generator->getLastMod()
            ];
        }
        return $links;
    }

    protected function _getSitemapStartTag()
    {
        return "
<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">
        ";
    }

    protected function _getSitemapEndTag()
    {
        return "
</sitemapindex>
        ";
    }

    public function getLastMod()
    {
        return false;
    }

}