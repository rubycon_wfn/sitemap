<?php

$sitemapRoute = \Settings::getConfigValue('sitemap/url_path') ?: 'sitemap';
Route::get($sitemapRoute . '.xml', '\WFN\Sitemap\Http\Controllers\SitemapController@index');
Route::get($sitemapRoute . '/{generator}.xml', '\WFN\Sitemap\Http\Controllers\SitemapController@view')->name('sitemap.view');
