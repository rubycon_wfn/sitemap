<?php

namespace WFN\Sitemap\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class SitemapController extends BaseController
{

    public function index()
    {
        $indexGenerator = new \WFN\Sitemap\Model\Index();
        return $this->_sendResponse($indexGenerator->getXml());
    }

    public function view($generator)
    {
        $generators = config('sitemap.generators');
        if(empty($generators[$generator])) {
            return abort(404);
        }
        
        if(\Settings::getConfigValue('sitemap/exclude-' . $generator)) {
            return abort(404);
        }

        $generator = new $generators[$generator]();
        if(!$generator instanceof \WFN\Sitemap\Model\Generator) {
            return abort(404);
        }
        
        return $this->_sendResponse($generator->getXml());
    }

    protected function _sendResponse($xmlContent)
    {
        return response($xmlContent)->header('Content-Type', 'text/xml');
    }

}